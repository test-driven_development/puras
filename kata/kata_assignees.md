| Вежба                 | Име и презиме      | имејл                        | Број индекса |
| --------------------- | ------------------ | ---------------------------- | ------------ |
| 100 doors             | Nemanja Trifunović | nemanja.trifunovic@rt-rk.com | E240/2022    |
| 12 Days of Xmas       |                    |                              |              |
| ABC Problem           | Раденко Михајловић | radenko.mihajlovic@rt-rk.com | E224/2022    |
| Align Columns         |                    |                              |              |
| Anagrams              |                    |                              |              |
| Array Shuffle         |                    |                              |              |
| Balanced Parentheses  |                    |                              |              |
| Best Shuffle          |                    |                              |              |
| Bowling Game          |                    |                              |              |
| Calc Stats            |Александар Кокиновић|                              | Е239/2022    |
| Closest To Zero       | Vanja Arbutina     |arbutinavanja064@gmail.com    | E2136/2022   |
| Combined Number       |Ivana Milosavljević |ivanamilosavljevic14@gmail.com| E2131/2022   |
| Count Coins           |                    |                              |              |
| Diff Selector         |                    |                              |              |
| Diversion             |                    |                              |              |
| Eight Queens          |                    |                              |              |
| Filename Range        |                    |                              |              |
| Fisher-Yates Shuffle  |                    |                              |              |
| Five Weekends         |                    |                              |              |
| Fizz Buzz             | Momcilo Krunic     | momcilo.krunic@labsoft.dev   | -            |
| Fizz Buzz Plus        |                    |                              |              |
| Friday 13th           |                    |                              |              |
| Game of Life          | Radomir Zlatkovic  | Radomir.Zlatkovic@rt-rk.com  | E247/2022    |
| Gray Code             | Stefan Marinkov    | marinkovstefan99@gmail.com   | E244/2022    |
| Group Neighbours      |                    |                              |              |
| Haiku Review          | Vuk Čabrilo        | vuk.cabrilo@rt-rk.com        | E222/2022    |
| Harry Potter          | Milica Vujanic     | Milica.Vujanic@rt-rk.com     | E258/2022    |
| ISBN                  |                    |                              |              |
| Knight's Tour         |                    |                              |              |
| LCD Digits            | Aleksa Stokanovic  | stokanovic.aleksa99@gmail.com| E215/2022    |
| Leap Years            | Luka Bilač         | luka.bilac@rt-rk.com         | E249/2022    |
| Levenshtein Distance  |                    |                              |              |
| Longest Common Prefix |                    |                              |              |
| Magic Square          |                    |                              |              |
| Mars Rover            |                    |                              |              |
| Mine Field            |                    |                              |              |
| Mine Sweeper          |                    |                              |              |
| Monty Hall            |                    |                              |              |
| Number Chains         |                    |                              |              |
| Number Names          |                    |                              |              |
| Phone Numbers         | Stefan Zec         | stefanzec@hotmail.rs         | E231/2022    |
| Poker Hands           |                    |                              |              |
| Prime Factors         |                    |                              |              |
| Print Diamond         |                    |                              |              |
| Recently Used List    |                    |                              |              |
| Remove Duplicates     | Dušan Stanišić     | milenkozfk70@gmail.com       | E246/2022    |
| Reordering            |                    |                              |              |
| Reverse Roman         | Ognjen Bošković    | Ognjen.Boskovic@rt-rk.com    | E251/2022    |
| Reversi               |                    |                              |              |
| Roman Numerals        |  Marina Vujičić    |  marinavujicic1@gmail.com    |  E2135/2022  |
| Saddle Points         |                    |                              |              |
| Tennis                |                    |                              |              |
| Tiny Maze             |                    |                              |              |
| Unsplice              |                    |                              |              |
| Vending Machine       |                    |                              |              |
| Wonderland Number     |                    |                              |              |
| Word Wrap             |                    |                              |              |
| Yatzy                 |                    |                              |              |
| Yatzy Cutdown         |                    |                              |              |
| Zeckendorf Number     |                    |                              |              |
